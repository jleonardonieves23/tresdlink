import { Component, OnInit } from '@angular/core';
import { CarService } from 'src/app/service/car.service';
import { Car } from '../interfaces/car';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  cars: Car[] = [];
  displayedColumns: string[] = [
    'model',
    'color',
    'doors',
    'price',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  constructor(private _carService: CarService) { }

  ngOnInit(): void {
    this.loadCars();
  }

  loadCars(){
    this._carService.getCars().subscribe(
      data => {
        this.cars = data
        this.dataSource = new MatTableDataSource(this.cars);
      }
    )
  }

  deleteCar(car: Car){
    let index: number = this.cars.findIndex(d => d === car);
    console.log("eliminar car",this.cars.findIndex(d => d === car));
    this.cars.splice(index,1)
    this.dataSource = new MatTableDataSource(this.cars);
    
  }

}
