import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ShareModule } from '../share/share.module';
import { DashboardComponent } from './dashboard.component';
import { StartComponent } from './start/start.component';
import { UserComponent } from './user/user.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CreatUserComponent } from './creat-user/creat-user.component';
import { CarComponent } from './car/car.component'; 


@NgModule({
  declarations: [
    DashboardComponent,
    StartComponent,
    UserComponent,
    NavbarComponent,
    CreatUserComponent,
    CarComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ShareModule
  ]
})
export class DashboardModule { }
