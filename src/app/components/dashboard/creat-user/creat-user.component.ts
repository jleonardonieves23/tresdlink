import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../interfaces/user';
import { UserService } from 'src/app/service/user.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-creat-user',
  templateUrl: './creat-user.component.html',
  styleUrls: ['./creat-user.component.css'],
})
export class CreatUserComponent implements OnInit {
  sexo: any[] = ['Femenino', 'Masculino'];
  form: FormGroup;
  titel: string = 'Crear Usaurio';
  size: number = 0;
  
  constructor(
    private fb: FormBuilder,
    private _userService: UserService,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _activatedRoute: ActivatedRoute
  ) {
    this.form = this.fb.group({
      user: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      sex: ['', Validators.required],
    });
    this._activatedRoute.params.subscribe((params) => {
      let id = params['id'];      
      if (id) {
        this.titel = 'Editar Usuario';
        this.lodForm(id);
      }
    });

  }

  ngOnInit(): void {    
  }

  back(){
    this._router.navigate(['/dashboard/usuario']);
  }

  saveSuser() {
    const user: User = {
      user: this.form.value.user,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      sex: this.form.value.sex,
    }; 
    
    if(this.titel !== 'Editar Usuario'){
      this._userService.addUser(user);
    } else {
      this._activatedRoute.params.subscribe((params) => {        
        this._userService.updateUser(user, params['id']);
      });
      
    }

    this._router.navigate(['/dashboard/usuario']);
    this._snackBar.open('Usario fue registrado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  lodForm(index: number) {

    const user = this._userService.getByUser(index);

    this.form = this.fb.group({
      user: [user?.user, Validators.required],
      firstName: [user?.firstName, Validators.required],
      lastName: [user?.lastName, Validators.required],
      sex: [user?.sex, Validators.required],
    });
  }

  onKeypressEvent(event: any){
    this.size = event.target.value.length ; 

  }
}
