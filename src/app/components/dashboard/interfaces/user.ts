export interface User {
    user: string,
    firstName: string,
    lastName: string
    sex: string
}