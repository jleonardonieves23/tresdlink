export interface Car {    
        model: string,
        color: string,
        doors: number, 
        price: number
}