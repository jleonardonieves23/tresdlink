import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { StartComponent } from './start/start.component';
import { UserComponent } from './user/user.component'; 
import { CreatUserComponent } from './creat-user/creat-user.component';
import { CarComponent } from './car/car.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, children: [
    { path: 'inicio', component: StartComponent },
    { path: 'carro', component: CarComponent },
    { path: 'usuario',  component: UserComponent },
    { path: 'crear-usuario', component: CreatUserComponent },
    { path: 'edita-usuario/:id', component: CreatUserComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
