import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../interfaces/user';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UserService } from 'src/app/service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  listUser: User[] = [];

  displayedColumns: string[] = [
    'user',
    'firstName',
    'lastName',
    'sex',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private _userService: UserService,
    private _snackBar: MatSnackBar,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.loadUser();
  }

  ngAfterViewInit() {
  }

  loadUser() {
    this.listUser = this._userService.getUser();
    this.dataSource = new MatTableDataSource(this.listUser);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  deleteUser(idenx: number) {
    this._userService.deleteUser(idenx);
    this.loadUser();

    this._snackBar.open('Usario fue eliminad con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  editUser(idenx: number) {
    this._router.navigate([
      '/dashboard/edita-usuario',
      idenx,
      'true',
      'usuario',
    ]);
  }
}
