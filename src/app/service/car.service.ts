import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Menu } from '../components/dashboard/interfaces/menu';
import { Car } from '../components/dashboard/interfaces/car';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient) { }

  getCars():Observable<Car[]> {
    return this.http.get<Car[]>('./assets/data/car.json')
  }
}
