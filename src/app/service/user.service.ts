import { Injectable } from '@angular/core';
import { User } from '../components/dashboard/interfaces/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  listUser: User[] = [
    { user: "grey26", firstName: "Grey", lastName: "Tovar", sex: "Femenino"},
    { user: "Jose25", firstName: "Jose", lastName: "Perez", sex: "Masculino"},
    { user: "leo265", firstName: "leonardo", lastName: "Nieves", sex: "Masculino"},
    { user: "Leonela24", firstName: "Leonela", lastName: "Gonzalez", sex: "Femenino"},
    { user: "Juan", firstName: "Juan", lastName: "Garcia", sex: "Masculino"}
];

  constructor(private http: HttpClient) { }

  getUser() {
    return this.listUser.slice();
  }

  getByUser(index: number){
    return this.listUser.find(e => index)
  }

  addUser(userNw: User){
    this.listUser.unshift(userNw)
  }

  editUser(){
    this.listUser.push();
  }

  deleteUser(i: number){
    this.listUser.splice(i, 1);
  }

  updateUser(user: User, index: number){
    this.listUser.splice(index, 1, user);
  }

}
