import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardModule } from './components/dashboard/dashboard.module';
import { StartComponent } from './components/dashboard/start/start.component';

const routes: Routes = [
  { path: '', redirectTo : 'dashboard', pathMatch: 'full'}, 
  { path: 'dashboard', component: DashboardComponent, children: [
    { path: '', component: StartComponent}
  ]},

  { path: 'dashboard', loadChildren: () => import('./components/dashboard/dashboard.module').then( d => DashboardModule) },
  { path: '**', redirectTo : 'dashboard', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
