# Prueba Teórica:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.2.

## 1.	¿Cómo funciona la detección de cambios en Angular y cómo se puede mejorar el rendimiento de una aplicación utilizando esta funcionalidad?

### Repuesta 
Cada vez que ocurre un evento en la aplicación, Angular realiza una comprobación de detección de cambios para validar si hay algún cambio en un valor y requiere actualización de la vista.
Este proceso es muy rápido, en el flujo unidireccional de los datos, Angular verificará cada componente que puedan haberse visto afectado para revisar si hubo cambios, y a gran medida esta funcionalidad estará ligada al rendimiento de la aplicación.
Para evitar esto podemos decirle a Angular en que momento realice estas acciones, utilizando la estrategia OnPush.
OnPush cambiará la estrategia de detección de cambios de un componente, esto quiere decir que cuando se emita una detección de cambios esta omitirá el componente si su valor de entrada no ha sido cambiado. Por ende todos los componentes dentro de este también se omitirán, con esto se reduce el proceso de detección de cambios


## 2.	¿Qué son las directivas estructurales en Angular y cómo se utilizan para manipular la estructura del DOM?

### Repuesta 
Las directivas estructurales corresponden a elementos en el HTML que permiten añadir, manipular o eliminar elementos del DOM. Estos elementos, en forma de atributos, se aplican a elementos huéspedes. Al hacer esto, la directiva hace lo que debe hacer sobre el elemento huésped y sus elementos hijos. Estas directivas son reconocibles debido a que están antecedidas por un asterisco (*) seguido del nombre de la directiva.

## 3.	¿Qué son los módulos en Angular y cómo se pueden utilizar para organizar una aplicación en diferentes características y funcionalidades?

### Repuesta 
En Angular, los módulos son una forma de organizar y separar la funcionalidad de una aplicación en diferentes bloques de código. Cada módulo tiene su propio contexto de ejecución y puede incluir componentes, servicios, directivas y otros artefactos de la aplicación.

## 4.	¿Qué son los guards en Angular y cómo se utilizan para proteger rutas de navegación en una aplicación?

### Repuesta 
Son interfaces que pueden indicar al enrutador si debe o no permitir la navegación a una ruta solicitada. Para utilizar un Guard, basta con ir a la definición de las rutas de la aplicación, una de las 4 guards, CanActivate, CanLoad, CanDeactivate, CanActivateChild.

## 5.	¿Qué son las animaciones en Angular y cómo se pueden utilizar para mejorar la experiencia del usuario?

### Repuesta 
La animación proporciona la ilusión de moviento de los elementos HTML cambiando de estilo con el tiempo, las animaciones pueden mejorar su aplicación y la experiencia del usuario:
Sin animaciones, las transiciones de la página web pueden parecer abruptas y discordantes. 
Motion mejora enormemente la experiencia del usuario, por lo que las animaciones les dan a los usuarios la oportunidad de detectar la respuesta de la aplicación a sus acciones.
Las buenas animaciones llaman intuitivamente la atención del usuario hacia donde se necesita.

# Prueba Práctica: 
## 1.	Cree un componente en Angular que muestre una lista de elementos, donde cada elemento tenga un botón que elimine el elemento correspondiente de la lista cuando se hace clic en él. Utilice la detección de cambios para mejorar el rendimiento de la aplicación.
## 2.	Cree un servicio en Angular que proporcione datos a un componente y otro servicio que permita a los usuarios agregar nuevos datos a través de un formulario. Asegúrese de que los datos se actualicen en tiempo real en el componente que los muestra. Utilice un BehaviorSubject para la comunicación entre el servicio y el componente.
## 3.	Cree un componente en Angular que muestre un formulario con dos campos de entrada, un botón de enviar y un contador que muestra cuántos caracteres se han ingresado en los campos de entrada en tiempo real. Utilice la programación reactiva y Reactive Forms para la validación de los campos de entrada.
## 4.	Cree un componente en Angular que muestre una tabla con datos de usuarios y un campo de búsqueda que filtre los resultados en tiempo real según lo que se ha ingresado en el campo de búsqueda. Utilice el patrón de Pipes para formatear los datos en la tabla y un Custom Pipe para la búsqueda.
## 5.	Cree un componente en Angular que muestre un gráfico de barras que se actualice en tiempo real a medida que los datos cambian. El componente debe permitir al usuario ordenar los datos por un campo determinado utilizando el patrón de Directives. Utilice el patrón de Lazy Loading para cargar el módulo que contiene el componente de gráfico de barras.

